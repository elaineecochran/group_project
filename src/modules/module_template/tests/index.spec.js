var assert = require('chai').assert;  // use chai 'assert' assertion library
import module_to_test from '../';

describe('Sample module tests:', () => {
    it('test1 should pass', () => {
        assert.equal(true, true);
    });

    it('test2 should fail', () => {
        assert.equal(true, false);
    });
});


// describe('Test Groups:', () => {
//     describe('Specific group #1', () => {
//         it('test1 should pass', () => {
//             assert.equal(true, true);
//         });

//         it('test2 should fail', () => {
//             assert.equal(true, false);
//         });
//     });
//         describe('Specific group #2', () => {

//         it('test1 should pass', () => {
//             assert.equal(true, true);
//         });

//         it('test2 should fail', () => {
//             assert.equal(true, false);
//         });
//     });
// });

// describe('Custom object module tests:', () => {
//     it('module should not be null', () => {
//         assert.isNotNull(module_to_test);
//     });
//     it('module should be an object', () => {
//         assert.isObject(module_to_test);
//     });
//     it('module should have a "name" property', () => {
//         assert.property(module_to_test, 'name');
//     });
//     it('module "name" property should default to "Tom"', () => {
//         assert.propertyVal(module_to_test, 'name', 'Tom');
//     });
//     it('module should have a method sayName()', () => {
//         assert.isFunction(module_to_test.sayName);
//     });
//     it('sayName method with no parameters should return "Hi, my name is Tom!', () => {
//         assert.equal(module_to_test.sayName(), 'Hi, my name is Tom!');
//     });
//     it('sayName method with parameter "Sun" should return "Hi, my name is Sun!', () => {
//         assert.equal(module_to_test.sayName('Sun'), 'Hi, my name is Sun!');
//     });
// });